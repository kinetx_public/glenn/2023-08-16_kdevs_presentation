INTRODUCTION
============

This directory is a high level overview of the KinetX KDEVS framework
that is targeted towards software developers.


HOW TO MAKE THE FILES
=====================

These instructions will work on a debian/ubuntu system.

You will need pandoc: and texlive:

```
$ sudo apt-get install pandoc
```

To generate the word document:

```
$ pandoc presentation.org -o presentation.docx
```

To generate a markdown version:

```
$ pandoc presentation.org --to gfm -o presentation.md
```
