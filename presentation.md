# KinetX KDEVS Presentation

# Agenda

- quick overview
- roadmap of gitlab repos
- the COTS that are used
- overview and discussion of architecture and vision

# Overview

## What is KDEVS?

- constellation sims, some sensor geometry, 30 days
- output: csvs
- minimial visualization: cesium
- aws: large scale runs
- some verification to stk (by KinetX and Northstar, which drove a lot
  of decisions -\> GMAT & SPICE)

## High Level Architecture

- ephemeris and attitude generation: GMAT
- SPICE (via SpiceyPy): sensor event detection
- run sims on aws to get geometrical event detections by sensors
- hand off event detections to NorthStar so they can do in-depth
  analysis to determine if sensor actually captures space objects, etc

## GMAT Ephemeris and Attitude

- NorthStar constellation satellite ephemeris and attitude
- space objects ephemeris
- use NAIF kernels for solar system bodies and high quality Earth
  gravitational model

## SPICE Sensor Geometric Event Detection

- create SPICE instrument kernal files (sensor shape, pointing)
- use SPICE, the GMAT outputs and kernel files to find geometric event
  detection
- output: more csvs
- handoff output to NorthStar

## Visualization

- cesium stuff; very rudimentary, was basically just for "does it look
  right"; no sensor cones, etc
- stk used for some targeted visualization (but not by Glenn)

## Run Simulations on Amazon Web Services (AWS)

- run massive number of sims on Ama
- briefly talk about this, as it wasn't my thing
- try and determine how sims were broken up

## Validation

- take a small subset of 1 sat and 1 object and run for a day in stk;
  would result in a handful of events (time, sat angle to object, etc)
  that were compared for accuracy to stk

## DEVS / KDEVS - Discrete Event Simulation

- not used in any of the Northstar sims because it wasn't needed
- DEVS useful for modeling different subsystems, components or events
  that fire at different or random times
- big vision was to have some hwil capability, with rest of sim
  framework providing environmental data (where it is in orbit,
  attitude, etc), while simulating particular spacecraft subsystems
- done in a hacky way at Spectrum Astro by Glenn for NFIRE, but never
  done at KinetX

# Gitlab

## Overview

- 91 git repos spread over 16 groups
- note there are NorthStar and BAMS project repos & deliverable repos in
  here

## Important Groups

### NorthStar Group

1.  Overview

    - <https://gitlab.com/kinetx_northstar>
    - group for repositories relating to NorthStar
    - very important group, all repos within are significant
    - this is the "official" repository for work done for NorthStar,
      sims, deliverables, etc
    - NorthStar has access to this repo

2.  Repos

    1.  <https://gitlab.com/kinetx_northstar/examples>

        - very well documented examples that were used in the
          verification of how we used GMAT + SPICE against stk
        - probably the most important repo, if you only pay attention to
          a single repo, it's probably this one
        - has python code for a complete GMAT + SPICE NorthStar sim
        - has stk scenario for same set up
        - <https://gitlab.com/kinetx_northstar/examples/SPICE_event_detection>
          - is the directory you want to look at for approach, how the
            validation occured and the python code is extremely well
            documented
          - does a space object detection and a ground target detection

    2.  <https://gitlab.com/kinetx_northstar/sims> - subgroub for sims
        for various scenarios

        1.  <https://gitlab.com/kinetx_northstar/sims/2019-04-04_trade_005>

            - the only trade study that was done using the more accurate
              combination of GMAT + SPICE
            - the other trades (001 - 004) used orekit which was found
              to have terrible event detecting

    3.  <https://gitlab.com/kinetx_northstar/aws>

        - lots of aws scripts for running the sims, I can't really speak
          to these, this was Brian Finney stuff

    4.  <https://gitlab.com/kinetx_northstar/doc>

        - the file within, KDEVS<sub>ICDv30</sub>.txt, documents the ICD
          file formats between KinetX and NorthStar
        - this is pretty important

    5.  <https://gitlab.com/kinetx_northstar/kernels>

        - NorthStar SPICE kernels such as frame kernels, instrument
          kernels and NAIF-provided kernels

### MUOS Group

1.  Overview

    - <https://gitlab.com/kinetx_muos>
    - small non-space, DEVS sim supporting MUOS
    - this was not a satellite sim
    - I think it was some radio interference modeling

2.  Repos

    1.  <https://gitlab.com/kinetx_muos/sim>

        - the actual repo containing code
        - if you want python DEVS code, use this as a starting place,
          especially as it has properly copyrighted files
        - I believe it is the latest version of the python DEVS code
          although for convenience copies of it were scattered about
          other reposs to make the repos more or less standalone
        - the "original" version of the python DEVS is in the

### Kinetx Simulation group

1.  Overview

    - <https://gitlab.com/kinetx_simulation> (group)
    - the original design and some implementation for the big picture
      sim / framework aka KDEVS
    - has various discussions throughout on what the big picture goals
      were, like multi-objective optimization and hardware-in-the-loop
    - some of this was scrapped because orekit was not up to being used
      for the sims that needed to be done; refer to the
      kinetx<sub>northstar</sub> group for what was done for NorthStar
      after scrapping orekit

2.  Repos

    1.  <https://gitlab.com/kinetx_simulation/python-devs>

        - the DEVS implementation KinetX; a fork of McGill PythonPDEVS
          (see COTS section)

    2.  <https://gitlab.com/kinetx_simulation/design>

        - repo containing the design
        - lots of good stuff in here, at least as far as design and
          vision
        - see
          <https://gitlab.com/kinetx_simulation/design/-/blob/master/doc/2016-12-14_whitepaper/simulation_capability_whitepaper.org?ref_type=heads>
          for an early discussion of the overall vision and architecture

### KinetX Forks

1.  Overview

    - <https://gitlab.com/kinetx_forks>
    - KinetX forks of some COTS tools with KinetX modifications,
      importantly the KinetX fork of PythonPDEVS

2.  Repos

    1.  <https://gitlab.com/kinetx_forks/PythonPDEVS>

        - the KinetX DEVS fork of McGill PythonPDEVS (this is the "DEVS"
          in "KDEVS")

### Glenn's NorthStar Group

1.  Overview

    - <https://gitlab.com/kinetx_glenn/northstar>
    - Glenn's electronic notebooks, works in progress, etc for NorthStar

# COTS

## GMAT - NASA

- <https://opensource.gsfc.nasa.gov/projects/GMAT/index.php>
- used for orbit propagation and satellite ephemeris
- has a GUI but it can also be run in command line mode, which is what
  was done with the NorthStar sims

## SPICE - NASA

- <https://naif.jpl.nasa.gov/naif/toolkit.html>
- used for event detecting (space and ground)
- needs lots of complicated kernel and instrument files to use, but very
  powerful
- they have a ton of documentation and training material on how to use
  it

## SpiceyPy - python wrapper for SPICE

- <https://spiceypy.readthedocs.io/en/main/>
- assumes you understand SPICE but very good example code, which was
  heavily leveraged

## Cesium - visualization

- <https://cesium.com/>
- spinoff of STK
- used the community version which is pretty limited (no sensor cones
  for example, at least at that time, I have no idea what's in their
  current community edition)
- the licensed version is very featureful, i.e, has sensor cones and
  pretty much anything STK can do visually

## CZML - json format for driving Cesium

- <https://github.com/AnalyticalGraphicsInc/czml-writer/wiki/CZML-Guide>

## czml - python library for Cesium

- <https://pypi.org/project/czml/>
- python wrapper library using CZML to drive Cesium

## McGill PythonPDEVS

- <http://msdl.uantwerpen.be/projects/DEVS/PythonPDEVS>
- open source pythone DEVS library
- KinetX forked this to make modeling more convenient
- our fork was used whenever DEVS was used (i.e,
  <https://gitlab.com/kinetx_muos>)

# High Level Architecture / Vision

| Capability                                              | Description / Purpose                           | Status          |
|---------------------------------------------------------|-------------------------------------------------|-----------------|
| development environment                                 | virtual machine dev environments                | completed       |
| satellite ephemeris & attitude; space objects ephemeris | GMAT                                            | completed       |
| sensor and sensor frame definitions                     | SPICE                                           | completed       |
| sensor event detection of space and ground targets      | SPICE                                           | completed       |
| visualization                                           | Cesium                                          | minimal support |
| DEVS framework                                          | KinetX fork of McGill PythonPDEVS               | completed       |
| run sims on cluster                                     | Amazon Web Services                             | initial support |
| multi-objective optimization (MOO)                      | optimize several system parameters via MOO sims | \-              |
| hardware in the loop simulation                         | test / validate hardware components with sim    | \-              |
| system in the loop simulation                           | test / validate enterprise systems with sim(s)  | \-              |

## Development Environment

- virtual machine development environment
- populated with development tools (python, editors, etc)
- populated with COTS (GMAT, SPICE, etc)
- prevent "it runs on my computer" symptom
- installation and configuration entire environment only took about 20
  minutes instead days doing it by hand
- meant to be used by non software developers like analysts and space
  system engineers

## Ephemeris and Attitude Generation

### Initially Used Orekit

- had to stop using orekit:
  - event detection missed large percentage of events
  - orbit propagation became very inaccurate after about 7 days
  - instrument modelling was very primitive
- searched for replacements, settled on GMAT and SPICE as they produced
  results very close to STK

### GMAT - Ephemeris and Attitude Generation

- NASA open source, kind of a poor man's STK
- generate satellite ephemeris and attitude
- generate space object ephemeris
- GUI driven but more importantly, can be run via command line, which
  what KDEVS does
- decently fast but we would generate these in an external step because
  they could be reused instead of constantly generating them
- one of the trade study variabilities would be the NorthStar
  constellation orbit parameters

### SPICE - Sensor Definition and Event Detection

- NASA open source
- define spacecraft and sensor frame kernels
- generate event detections between sensors and Earth or space targets
- SPICE can be intimidating to use but it is very powerful and very
  accurate (if you know how to configure and use it correctly)
- very low level though that you have to wire different pieces together
- very well documented by NASA with exceptional documentation and
  separate training material
- SpiceyPy: python wrapper to SPICE
  - very featureful and also well documented with great examples
- tons of functionality but KDEVS only used it for event detection:
  - satellite sensor to space object
  - satellite sensor to ground target
- SPICE has lots of flexibility in defining sensors; NorthStar sensors
  were easily modeled after some learning ramp-up time of figuring out
  how the sensor kernel files worked

## Simulation Workflow

### GMAT

- python workflow:
  - input: initial satellite and space object states and orbit
    parameters
    - NorthStar provided NorthStar constellation parameters to use for
      various trade studies
    - NorthStar created a subset of NORAD catalog objects to use as
      space targets (initial states for these were obtained from
      space-track.org)
  - creates GMAT configuration file with initial states and desired
    output format and specifies various NAIF kernel files for solar
    system bodies
  - runs GMAT commnad line variant
  - output:
    - satellites: csv files of timestepped ephemeris and attitude
    - space objects: csv files of timestepped ephemeris

### SPICE

- python workflow:
  - input: kernel files used in GMAT portion, ephemeris and attitude csv
    files generated from GMAT, instrument definition files
  - run the SPICE event detection function via SpiceyPy
  - output: satellite, sensor, target ids, sun angles, positions
- NorthStar would then take this output and do further post processing
  determing if objects are bright enough to be captured by sensor, etc

### ICD for Files

- <https://gitlab.com/kinetx_northstar/doc/-/blob/master/KDEVS_ICD_v30.txt?ref_type=heads>
- defines all the input and output files discussed above

### Documented Example of Workflow

1.  GMAT / SPICE python workflow

    - <https://gitlab.com/kinetx_northstar/examples/spice_event_detection>
    - tutorial example, deeply documented, explaining entire process
      from kernel generation, ephemeris generation, to event detection
      for both space and ground targets
    - also validates results against the following STK scenario
    - this is probably the most important repo

2.  STK scenario

    - <https://gitlab.com/kinetx_northstar/examples/spice_event_detection_stk>
    - all the files necessary to run the exact same scenario in STK

## Visualization

### Cesium

- a huge library of javascript from spinoff of STK companry
- community (i.e., "free") version allows basic visualization
  - but no sensor codes or line of sight lines
- lincensed version has pretty much STK equivilent visualization
  - lots of bells & whistles, including sensor cones
- czml: Cesium defined json specification for driving Cesium
- pretty cumbersome usage:
  - take sim outputs
  - use czml python library to create Cesium json input
  - run Cesium in web browser in "movie" mode

### Advice

- try and find an alternative

## DEVS Framework

- DEVS: a specific discrete event simulation theory and framework
- it's a very mathematical theory (based on automata theory) with
  boatloads of proofs and mathematics proving all sorts of properties of
  the framework, like for example, that it can simulate anything and
  that it is "correct"
  - it hasn't been proven but it is thought to be the minimal theory of
    simulation that is needed; consider it the quantum mechanics of
    simulation
    - minimal because other formalisms such as Petri nets and others can
      be modeled in DEVS while the reverse is not true
- solves the "I have models that run at different rates, how do I model
  all of this?" problem
  - i.e., if you have a 5Hz model and a 7Hz model, the naive approach is
    to run the sim at 35Hz, very inefficent
- can be implemented in any language
- in terms of customers, was only used in
  <https://gitlab.com/kinetx_muos> group
  - this was not a space sim
- DEVS was not used in any of the NorthStar simulations because it
  wasn't needed
- only use DEVS when you need true discrete event simulation, especially
  with different model rates and/or random events
  - but if you have this situation, it is THE most straightforward way
    of modeling and simulating stuff like this
  - trust me, bro
- unfortunately learning DEVS is a tough road, there's lots of academic
  resources, books, papers, etc, but at least while I was active, no
  "learner" material
- theory discovered by Dr. Bernard P. Ziegler, who for a long time was
  at University of Arizona
  - <https://d.lib.ncsu.edu/computer-simulation/videos/bernard-p-zeigler-interviewed-by-richard-e-nance-zeigler/>
- finding good resources is difficult, here's some
  - <https://acims.asu.edu/>
    - the focal point of DEVS research and application, jointly run by
      ASU and UofA
    - lots of resources here, software, material, etc
  - <https://rtsync.com/>
    - commercial company based in Phoenix for doing DEVS government and
      commercial work
    - you might consider talking to them
- there are tons of DEVS implementations, usually university projects
  - varying quality
  - usually written in support of specific research instead of general
    applicability
  - the McGill PythonPDEVS
    <http://msdl.uantwerpen.be/projects/DEVS/PythonPDEVS> is the best
    for casual users or those that don't want to implement it themselves
  - once you understand DEVS, it's not too difficult to write your own
    implementation, this could be advantageous if for some reason one of
    the open source projects doesn't quite work out

## Running Sims on a Cluster

- obvious advantages when needing to run massive amounts of simulation
  runs
- KinetX chose AWS for pragmatic and cost reasons, but at the time it
  was a pretty hostile environment for running sims
  - for example, a traditional file system is very burdemsome to use in
    AWS
  - monitoring and managemnt in AWS (at the time) was also hard to do
    - anecdote, for some reason there were some sim runs that left some
      AWS virtual machines running after their sims completed and after
      they were "shutdown", but they were never shutdown and continued
      to be billed – nasty
- advice: consider buying your own small bazillion core, bazillion
  memory cluster
- tons of management and coordination scripts for AWS in
  <https://gitlab.com/kinetx_northstar/aws>
  - it's all a bunch of hacky shell scripts, though
  - specific virtual machines were brought up and scripts "manually"
    assigned sim jobs to run on them
  - there was no sim schedule, unfortunately
    - no time to really research this out and find a toolset that would
      do this or write our own
    - advice: find a good toolset that can manage this for you; you
      might need to look in the high performance computing community for
      the toolsets though
- one of the reasons this was all attempted was to pave the way for
  implementing multi-objective optimization

## Multi-Objective Optimization

- a big step over running massive numbers of simulations
- but use that computing power to run multi-objective optimization to
  simultaneously optimize several variables
- i.e., in the NorthStar case, instead of running a handful of fixed
  scenarios,
  - come up with an objective function, say that outputs a number
    representing the number and profit of "high quality" data
    collections
  - have a MOO algorithm control running the overal cluster to run sims
    and search through results of each simulation run
    - vary orbit parameters
    - vary number of satellites
    - different sensors, different placement on satellite
    - etc
  - if your objective function is any good (WHICH IS THE HARD PART!), it
    will tell you what value of the overal varying parameters optimizes
    your objective function
- requires massive amount of simulation runs, hence the need for running
  on a cluster
- unfortunately never got past the "gee this would be real cool if we
  could do this" stage
- overall feeling on this: technically setting this up is challenging
  but doable; deriving useful business value from this totally depends
  on identifying valid parameter that can in reality be realized
  physically (i.e., specific orbit parameters, number of satellites),
  AND coming up with a valid objective function

## Hardware-in-the-loop Simulation

- done with DEVS in an adhoc way with an actual hardware satellite
  simulator at Spectrum Astro (the NFIRE experience referenced in some
  KinetX slides)
  - I believe I was the first person in the world to have actually done
    this, based on a conversation I had with Ziegler
- proved the usefulness of being able to do this
- never got to the stage of thinking what a productization of
  formalizing the ability the support this
- however, everyone seems to like the idea of using simulation to go
  smoothly from initial concepts and requirements evalution, through
  details simulation based design, through testing and validation of
  physical components or systems using the same simulation framework or
  theory (i.e, DEVS)

## System-in-the-loop Simulation

- like the hardware-in-the-loop situation but on a bigger, enterprise
  level
- so the "killer idea" that most people miss, is that DEVS is not only a
  theory of simulation, it is a theory of systems engineering
- if you do your systems engineering using DEVS, either as a modeling
  formalism (instead of say, SysML) and then as a simulation formulism,
  YOU CAN GROW YOUR SYSTEM FROM ANALYSIS, REQUIREMENTS, DESIGN,
  IMPLEMENTATION AND TESTING (THROUGH ALL PHASES) USING THE SAME
  FORMALISM, I.E., DEVS
  - in my opinion, this is more powerful than a SysML approach
  - but I'm very much in the minority on this
  - but it worked on NFIRE; spectacularly
- I call this the universal architecture
